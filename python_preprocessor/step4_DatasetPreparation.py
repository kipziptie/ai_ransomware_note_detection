from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
import os, pprint, pickle
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

CORRELATION_TUNING = .90
TEST_TRAIN_SPLIT_TRAIN_SIZE = 0.33 # when a float is used, the value represents fraction used for testing..
VECTOR_LENGTH = 20  # Default is 20. 40 is a fun number too.
STATIC_RANDOM_TEST_TRAIN = True
IMAGES = True
def getDirContents(dir):
    return sorted(os.listdir(dir))

BOW_DIR = os.path.dirname("./bow/")
CORPUS_DIR = os.path.dirname("./corpus/")
def readBow(files):
    for f in files:
        content = ''
        filepath = os.path.join(BOW_DIR, f)
        print(filepath)
        BoW = np.load(filepath, allow_pickle=True)
        print("Here, we have the Bag of Words BoW vector representation of my CORPUS:", f)
        pprint.pprint(BoW)

#readBow(getDirContents(BOW_DIR))

def getCorpusAsLists(file):
    outfile = []
    with open(file, "rb") as f:
        corpus = pickle.load(f)
    corpus = [str(item) for item in corpus]
    return corpus

def getCorpusAsLines(file):
    outfile = []
    with open(file, "rb") as f:
        corpus = pickle.load(f)

    for line in corpus:
        outfile.append(' '.join(str(item) for item in line if not isinstance(item, int)))
    outfile = [str(item) for item in outfile]
    return outfile

########################################################
# readCorpus() - accepts a list of Bag-of-Words vectors representing variants of the same corpii and assembles
# Training and Testing data for ingest by Scikit learn ML Modules.
#
# Purpose - Scikit learn models require 4 distinct variables
#     X_train - Vector for training
#     X_test - Vector for testing
#     y_train - correct answer for training the model
#     y_test - correct answer for measuring model accuracy
#
# This function also incorperates some feature reduction by analyzing the correlation the vector components and drops
# the highly correlated features. This is a tunable variable worth exploring.
########################################################
def readCorpus(files):
    test_train_splits = []
    for f in files:
        print("Processing: ", f)
        if str(f).endswith("Negative"):
            print("skipping ", f)
            continue

        filepath = os.path.join(CORPUS_DIR, f)
        print(filepath)
        corpus = getCorpusAsLists(filepath)
        #print(corpus)
        negativeCorpus = getCorpusAsLists(os.path.join(CORPUS_DIR, "corpusNegative"))
        #print("\nnegativeCorpus\n", negativeCorpus)

        #corpus = ['this is the first sentence', 'this is the second sentence', 'this is the third sentence', 'this is the fourth sentence', 'this is the fifth sentence']
        positiveDataset = pd.DataFrame({'document': corpus, 'label':1}, dtype=str)
        negativeDataset = pd.DataFrame({'document': negativeCorpus, 'label':0}, dtype=str)
        positiveDataset = positiveDataset.append(negativeDataset, ignore_index=True)
        #positiveDataset.concat()
        #positiveDataset = positiveDataset.append(negativeDataset)
        print("Positive Dataset: \n", positiveDataset)
        #print(dataset.dtypes)
        #print(dataset[0:10])

        matrix = CountVectorizer(max_features=VECTOR_LENGTH)

        #Define the X values
        #X = matrix.fit_transform(positiveDataset)
        X = matrix.fit_transform(corpus + negativeCorpus).toarray()
        headings = matrix.get_feature_names_out()
        print(list(headings))
        print(X)

        corrmatrix = pd.DataFrame(X, columns=headings)
        correlation = corrmatrix.corr()
        print(correlation)
        plt.figure(figsize=(15, 12))
        sns.heatmap(correlation)
        plt.suptitle("Correlation heatmap of word vectors: Before Pruning")


        cor_thr = CORRELATION_TUNING # A variable to play with for tuning..
        print('Shape before feature reduction: ', corrmatrix.shape)
        corr_matrix = corrmatrix.corr().abs()
        upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > cor_thr)]
        print('Features to drop')
        print(to_drop)
        for i in to_drop:
            corrmatrix.drop(i, axis=1, inplace=True)
        print('Shape after feature reduction: ', corrmatrix.shape)
        plt.figure(figsize=(15, 12))
        sns.heatmap(corrmatrix.corr())
        plt.suptitle("Correlation heatmap of word vectors: After Pruning")

        if IMAGES == True:
            plt.show()

        print("Remaining Columns after feature reduction:\n", list(corrmatrix.columns))

        #X = matrix.fit_transform(dataset.document)
        Y = positiveDataset.label
        X = corrmatrix
        print("##### X #####\n", X)
        print("##### Y #####\n", Y)
        print(X.shape)

        if STATIC_RANDOM_TEST_TRAIN == True:
            X_train, X_test, y_train, y_test = train_test_split(X, Y, random_state=42, train_size=TEST_TRAIN_SPLIT_TRAIN_SIZE)
        else:
            X_train, X_test, y_train, y_test = train_test_split(X, Y, train_size=TEST_TRAIN_SPLIT_TRAIN_SIZE)

        splits = { "name":f,
                   "headings":list(X.columns),
                  "X_train":X_train,
                  "X_test":X_test,
                  "y_train":y_train,
                  "y_test":y_test}
        print("Appending dataset: ", filepath)
        test_train_splits.append(splits)

    return(test_train_splits)
print(getDirContents(CORPUS_DIR))
datasets = readCorpus(getDirContents(CORPUS_DIR))

def outputDatasets(dataset, name):
    outfile = dataset
    dir = os.path.join("scikit", name)

    with open(dir, "wb") as f:
        pickle.dump(outfile, f)

outputDatasets(datasets, "scikit_test_train_splits")

print("DONE!")