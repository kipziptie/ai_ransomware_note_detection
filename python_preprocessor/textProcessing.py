import os, re
from myGlobals import *
from curses.ascii import isprint

########################################################
# cleanFiles() - Strips non-printing characters and newlines from an input RAW text file.
#
# Purpose - convert non-specific encodings into a uniform output.
########################################################
def cleanFiles(files, negativeSamples=False, path=''):
    for f in files:
        content=''

        if not path:
            if negativeSamples == False:
                filepath = os.path.join(RAW_NOTE_DIR, f)
            elif negativeSamples == True:
                filepath = os.path.join(NEGATIVE_NOTE_DIR, f)
            print(filepath)
        else:
            filepath = os.path.join(path, f)
        with open(filepath, "r", encoding="utf8") as file:
            try:
                # Get the raw file content in text format
                content = file.read()

                if f.endswith(".txt"):
                    # strip all characters which are not copnsidered "printable" e.g. NULL /x00. Also Mark all URLs with
                    # the special ##URL## constant
                    #cleanedContent = markURLs(removeNewLines(printable(content)))
                    cleanedContent = markSpecialVariables(removeNewLines(printable(content)))
                elif ( f.endswith(".html") or f.endswith(".hta") or f.endswith(".htm") ) and INCLUDE_HTML_PROCESSING == True:

                    cleanedContent = markURLs(removeNewLines(printable(htmlToText(content))))

                #print("Raw Content:\n", content)
                #print("Cleaned Content:\n", cleanedContent)
            except UnicodeDecodeError as e:
                print("EXCEPTION: Failed to read ", filepath)
                print(e)
                # Add file reference to list of skipped files to try later
                SKIPPED.append(filepath)
                pass
            else:
                if negativeSamples == False:
                    print("  Writing positive sample...")
                    writeOutfile(cleanedContent)
                elif negativeSamples == True:
                    print("  Writing negative sample...")
                    writeOutfile(cleanedContent, True)


################################
# markURLs() - A function to standardize all URLs to a single marker, ##URL##, surrounded by whitespace.
#
# Purpose: to standardize the URL features to a single constant value
# args:     input - a string representing the contents of a file
#           returns - a string with ##URL## constant inserted in place of all substrings which are formatted like a URL
################################
def markURLs(input):
    # thanks to https://urlregex.com/ for the magic regex
    URL_REGEX = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    return(re.sub(URL_REGEX, " ##URL## ", input))

def markEMAILs(input):
    # thanks to https://emailregex.com/ for the magic regex
    EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    return(re.sub(EMAIL_REGEX, " ##EMAIL## ", input))

def markSpecialVariables(input):
    return (markURLs(markEMAILs(input)))

def removeNewLines(input):
    return ''.join(char for char in input.replace("\n", " ")).lower()

################################
# printable() - A bad character stripping function to remove NULL chars (and others which are not human-readable output)
#             - except newlines. Preserve newlines.
#
# Purpose: to remove characters which should not be seen by the machine learning model.
# args:     input - a string representing the contents of a file
#           returns - a string with null characters removed
################################
def printable(input):
    return ''.join(char for char in input if isprint(char.replace("\n", " ")))


def writeOutfile(content, negativeContent=False):
    if negativeContent== False:
        dir = CLEAN_NOTE_DIR
    elif negativeContent == True:
        dir = CLEAN_NEGATIVE_SAMPLES_DIR
    existing_notes = os.listdir(dir)
    index = len(existing_notes)
    print("    Files already present: ", index)
    path=os.path.join(dir, str(index))
    with open(path, "w") as file:
        for item in content:
            file.write(item)
    print("    FILE WRITTEN.\n")
def getDirContents(dir):
    return os.listdir(dir)

################################
# getTextFiles() - A function to select all files that end in .txt
#
# Purpose: to select only files that are labeled as .txt
# <TODO>This function needs a companion to convert non-text files to compatible text files
################################
def getFilesByExtension(files, extension):
    text_files = []
    html_files = []
    for file in files:
        if file.endswith(".txt"):
            #print(file)
            #print(os.path.join(RAW_NOTE_DIR, file))
            text_files.append(file)
        elif file.endswith(".html") or file.endswith(".hta") or file.endswith(".htm"):
            html_files.append(file)
    if extension == ".txt":
        return(text_files)
    elif extension == ".html" or extension == ".htm" or extension == ".hta":
        return(html_files)


def deleteDirectory(dir):
    for file in os.listdir(dir):
        os.remove(os.path.join(dir, file))