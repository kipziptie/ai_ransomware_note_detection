import pickle, os, time
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import joblib
import seaborn as sns
import numpy as np
import pandas as pd

DEBUG = True
IMAGES = False
SVM_TRAIN = False
SVM_GRID_SEARCH = True # Doesn't run unless SVM_TRAIN is also true.
DT_TRAIN = True
EXPERIMENTAL_SCORER = False # don't enable. it's a silly idea.


### Global Program Logic ##
FIRST_RUN = True # this var is a hacky way to track the first loop of execution of this program.
                 # it's used by getScikitSplits() function. <todo> write better code. </todo>
def getDirContents(dir):
    return sorted(os.listdir(dir))

def getSplits(file):
    with open(file, "rb") as f:
        splits = pickle.load(f)
    return splits

SCIKIT_DIR = os.path.dirname("./scikit/")

## This function sets a global variable whenever called.
scikitFiles = getDirContents(SCIKIT_DIR)
def setFilesInScikitDir():
    global scikitFiles
    scikitFiles = getDirContents(SCIKIT_DIR)
    print(scikitFiles)
def deleteFile(filepath):
    os.remove(filepath)
    print("FILE DELETED: ", filepath)

def popSplitFromJoblist():
    deleteFile(os.path.join(SCIKIT_DIR, scikitFiles[0]))
    setFilesInScikitDir()
def getScikitSplits(file):
    #global FIRST_RUN
    #setFilesInScikitDir()
    filecount = len(file)
    filepath = os.path.join(SCIKIT_DIR, file[0])


    # if FIRST_RUN == False:
    #     deleteFile(filepath)
    #     setFilesInScikitDir()
    #     filepath = os.path.join(SCIKIT_DIR, scikitFiles[0])

    if filecount == 0:
        print("NO FILES PRESENT IN SCIKIT DIR. DID YOU RUN STEP #4?")
        exit(-1)
    # elif filecount == 1: # if there is only 1 file in splits dir...
    #     return splits
    # else:
    #     # time to join a bunch of pickled splits into 1 mega thing...
    #     # it should be as easy as calling .append since the structures are all the same and the headings are baked
    #     # into each split (THANK YOU PANDAS DATAFRAMES!)
    #     for f in file:
    #         filepath = filepath = os.path.join(SCIKIT_DIR, f)
    #         print("appending: ", filepath)
    #         splits.append(getSplits(filepath))
    #     for split in splits:
    #         print("##SPLIT##")
    #         print(split)
    #     return splits
    splits = getSplits(filepath)
    #FIRST_RUN = False
    return splits
def gaussianClassifier(data):
    print("  Gaussian Classifier")
    classifier = GaussianNB()
    classifier.fit(data["X_train"], data["y_train"])

    #Predict Class
    y_pred = classifier.predict(data["X_test"])

    # Accuracy
    accuracy = accuracy_score(data["y_test"], y_pred)
    print("    Accuracy: ", accuracy)

def plot_confusion_matrix(Y_real, Y_pred, title=None):
    labels=np.unique(Y_real)
    nc=len(labels)
    data=np.zeros((nc,nc))
    pairs=zip(Y_real,Y_pred)
    pairs=list(pairs)
    for p in pairs: data[np.where(labels==p[0]),np.where(labels==p[1])]+=1
    row_total=np.sum(data,axis=1)
    data=data/row_total.reshape((nc,1))
    sns.set(color_codes=True)
    plt.figure(1, figsize=(9, 6))
    plt.title('Confusion Matrix') if title is None else plt.title(title)
    sns.set(font_scale=1.4)
    ax = sns.heatmap(data, annot=True, cmap='Blues', cbar_kws={'label': 'Scale'})
    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)
    ax.set(ylabel='True Label', xlabel='Predicted Label')
    if IMAGES == True:
        plt.show()
def build_results_array(acc_train, pre_train, rec_train, acc_test, pre_test, rec_test, t_train, t_eval):
    if DEBUG == True:
        print("results array:\n", acc_train, pre_train, rec_train, acc_test, pre_test, rec_test, t_train, t_eval)
    return([acc_train, pre_train, rec_train, acc_test, pre_test, rec_test, t_train, t_eval])

########################################################
# decisionTrees() - Builds a support vector machines ML model
#
# Args:
#     1 - a scikit learn test train split (X train, X Test, Y train, Y test)
#     2 - Depth, an integer to set how deep the tree should be
# Purpose - allows for easy training of an Decision Trees model using a given dataset
########################################################
def decisionTrees(data, depth):
    print('\n#################\n# DT evaluation #\n#################')
    model_dt = DecisionTreeClassifier(criterion='entropy', max_depth=depth)
    start_time = time.time()
    model_dt.fit(data["X_train"], data["y_train"].values.ravel())
    time_train_tree = time.time() - start_time
    print("    Training time: ",time_train_tree)
    fig = plt.figure(figsize=(40, 20))
    _ = tree.plot_tree(model_dt, feature_names=data["headings"], class_names=model_dt.classes_,
                       filled=True, fontsize=12)
    if IMAGES == True:
        plt.show()
    start_time = time.time()
    Y_train_pred = model_dt.predict(data["X_train"])
    Y_test_pred = model_dt.predict(data["X_test"])
    time_eval_tree = time.time() - start_time

    acc_train = accuracy_score(data["y_train"], Y_train_pred)
    acc_test = accuracy_score(data["y_test"], Y_test_pred)
    rec_train = recall_score(data["y_train"], Y_train_pred, average="binary", pos_label="1")
    rec_test = recall_score(data["y_test"], Y_test_pred, average="binary", pos_label="1")
    pre_train = precision_score(data["y_train"], Y_train_pred, average="binary", pos_label="1")
    pre_test = precision_score(data["y_test"], Y_test_pred, average="binary", pos_label="1")
    t_train = time_train_tree / float(data["y_train"].shape[0])
    t_eval = time_eval_tree / (float(data["y_train"].shape[0]) + float(data["y_test"].shape[0]))

    print('Overall accuracy training: ', acc_train)
    print('Overall accuracy test: ', acc_test)
    print('Recall training: ', rec_train)
    print('Recall Test: ', rec_test)
    print('Precision Train: ', pre_train)
    print('Precision Test: ', pre_test)
    print('Training time (seconds per sample): ', t_train)
    print('Evaluation time (seconds per sample): ', t_eval)

    dt_result_arr = build_results_array(acc_train, pre_train, rec_train, acc_test, pre_test, rec_test, t_train, t_eval)

    if IMAGES == True:
        plot_confusion_matrix(data["y_train"].values.ravel(), Y_train_pred, title='Decision tree - Training')
        plot_confusion_matrix(data["y_test"].values.ravel(), Y_test_pred, title='Decision tree - Testing')

    # <TODO> joblib currently overwrites the model with the most recent..
    joblib.dump(model_dt, 'model_dt.joblib')

    return(dt_result_arr)

from sklearn.metrics import fbeta_score, make_scorer
def myScorer(y_true, y_pred):
    print("hello")
    return(precision_score(y_true, y_pred, average="binary", pos_label="1"))

########################################################
# svm() - Builds a support vector machines ML model
#
# Args:
#     1 - a scikit learn test train split (X train, X Test, Y train, Y test)
#
# Purpose - allows for easy training of an SVM model using a given dataset
########################################################
def svm(data):
    print('\n##################\n# SVM evaluation #\n##################')
    model_svm = SVC(kernel='linear', gamma='scale')
    start_time = time.time()
    model_svm.fit(data["X_train"], data["y_train"].values.ravel())
    time_train_svm = time.time() - start_time
    score = make_scorer(myScorer, greater_is_better=True)
    if SVM_GRID_SEARCH == True:
        # defining parameter range
        param_grid = {'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000],
                      'gamma': [3, 2, 1, 0.1, 0.01, 0.001, 0.0001, 0.00001],
                      'kernel': ['sigmoid'],
                      'class_weight':['balanced']}#,
                      #'class_weight': [{"0":3, "1":1},{"0":2, "1":1}, {"0":1.5, "1":1},{"0":1, "1":1},{"0":1, "1":1.5}, {"0":1, "1":2}, {"0":1, "1":4}]}


        if EXPERIMENTAL_SCORER == True:
            grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=3, scoring=score)
        else:
            grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=3, n_jobs=-1)
        start_time = time.time()

        # fitting the model for grid search
        grid.fit(data["X_train"], data["y_train"].values.ravel())
        time_svm_gridsearch = time.time() - start_time
        # print best parameter after tuning
        print(grid.best_params_)

        # print how our model looks after hyper-parameter tuning
        print(grid.best_estimator_)

    # Evaluation section - If grid search is enabled, the model will brute force the optimal constants
    if SVM_GRID_SEARCH == True:
        start_time = time.time()
        Y_train_pred = grid.predict(data["X_train"])
        Y_test_pred = grid.predict(data["X_test"])
        time_eval_svm = time.time() - start_time
    else:
        start_time = time.time()
        Y_train_pred = model_svm.predict(data["X_train"])
        Y_test_pred = model_svm.predict(data["X_test"])
        time_eval_svm = time.time() - start_time

    acc_train = accuracy_score(data["y_train"], Y_train_pred)
    acc_test = accuracy_score(data["y_test"], Y_test_pred)
    rec_train = recall_score(data["y_train"], Y_train_pred, average="binary", pos_label="1")
    rec_test = recall_score(data["y_test"], Y_test_pred, average="binary", pos_label="1")
    pre_train = precision_score(data["y_train"], Y_train_pred, average="binary", pos_label="1")
    pre_test = precision_score(data["y_test"], Y_test_pred, average="binary", pos_label="1")
    t_train = time_train_svm / float(data["y_train"].shape[0])
    t_eval = time_eval_svm / (float(data["y_train"].shape[0]) + float(data["y_test"].shape[0]))

    print('Overall accuracy training: ', acc_train)
    print('Overall accuracy test: ', acc_test)
    print('Recall training: ', rec_train)
    print('Recall Test: ', rec_test)
    print('Precision Train: ', pre_train)
    print('Precision Test: ', pre_test)
    print('Training time (seconds per sample): ', t_train)
    print('Evaluation time (seconds per sample): ',t_eval)

    svm_result_arr = build_results_array(acc_train, pre_train, rec_train, acc_test, pre_test, rec_test, t_train, t_eval)

    if IMAGES == True:
        plot_confusion_matrix(data["y_train"].values.ravel(), Y_train_pred, title='SVM - Training')
        plot_confusion_matrix(data["y_test"].values.ravel(), Y_test_pred, title='SVM - Testing')

    # <TODO> joblib currently overwrites the model with the most recent..
    joblib.dump(model_svm, 'model_svm.joblib')





    return(svm_result_arr)
def write_csv(in_dataframe, outfile):
    print("hello")
    path = os.path.join("step5_results", outfile)
    with open(path, "a") as file:
        in_dataframe.to_csv(file)
    print("    FILE WRITTEN.\n")

######################################################################
#                                                                    #
# LAZY MAIN - Just a buncha commands at the bottom. Deal with it. <3 #
#                                                                    #
######################################################################

# Constants for main
DT_RESULTS = []
SVM_RESULTS = []

while len(getDirContents(SCIKIT_DIR)) != 0:
    # looping through multiple split files strategy: Copy them to the running dir. After the run, delete the current file.
    # the next lowest file will be selected. It's a total hack - but it's also a WIN.
    setFilesInScikitDir()
    scikit_test_train_data = getScikitSplits(scikitFiles)
    #print(scikit_test_train_data)

    if DT_TRAIN == True:
        for i in range(3, 4):
            print("################################\nRun #",i,"\n################################")
            for dataset in scikit_test_train_data:
                if DEBUG == True:
                    print("\n### ",dataset["name"], " ###")
                    print(dataset)

                vals = list([char for char in dataset["name"] if char.isnumeric() ])
                dataset_number = "".join(vals)
                #gaussianClassifier(dataset)
                # <todo> this DT depth variable is another tunable arg
                # Train the Decision Trees model using the current dataset
                DT = decisionTrees(dataset, i)

                # insert the current 'tuning run index' into the result array
                DT.insert(0, i)

                # Insert the name of the corpus this current result array represents
                DT.insert(1, int(dataset_number))

                # Append this run to the big table of Decision Tree executions
                DT_RESULTS.append(DT)

                # Make a pretty pandas dataframe out of the executions table. Label it with headers.
                DT_dataframe = pd.DataFrame(np.array(DT_RESULTS), columns=['run_index','corpus_number','acc_train', 'pre_train', 'rec_train', 'acc_test', 'pre_test', 'rec_test', 't_train', 't_eval'])

                if DEBUG == True:
                    print("\n###########\n# RESULTS #\n###########")
                    print("Raw Results:")
                    for row in DT_RESULTS:
                        print("  ", row)
                    print("\nPandas Dataframe of results:")
                    print(DT_dataframe)
                write_csv(DT_dataframe, "DT.csv")

    if SVM_TRAIN == True:
        # Train the SVM model, using each corpus.
        for dataset in scikit_test_train_data:
            print(dataset["name"])
            #if DEBUG == True:
                #print("\n### ",dataset["name"], " ###")
                #print(dataset)

            vals = list([char for char in dataset["name"] if char.isnumeric() ])
            dataset_number = "".join(vals)
            SVM = svm(dataset)
            # Insert the name of the corpus this current result array represents
            SVM.insert(0, int(dataset_number))

            # Append this run to the big table of SVM executions
            SVM_RESULTS.append(SVM)

            # Make a pretty pandas dataframe out of the executions table. Label it with headers.
            SVM_dataframe = pd.DataFrame(np.array(SVM_RESULTS),
                                         columns=['corpus_number', 'acc_train', 'pre_train', 'rec_train',
                                                  'acc_test', 'pre_test', 'rec_test', 't_train', 't_eval'])
            write_csv(SVM_dataframe, "SVM.csv")
    popSplitFromJoblist()



