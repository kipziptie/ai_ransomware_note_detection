import os

### GLOBALS ###
RAW_NOTE_DIR=os.path.dirname("./rawNotes/")
#RAW_NOTE_DIR=os.path.dirname("./debug_notes/")
CLEAN_NOTE_DIR=os.path.dirname("./cleanedNotes/")
NEGATIVE_NOTE_DIR=os.path.dirname("./negativeSamples/")
CLEAN_NEGATIVE_SAMPLES_DIR=os.path.dirname("./cleanedNegativeSamples/")
INCLUDE_HTML_PROCESSING = False

SKIPPED = []
###############