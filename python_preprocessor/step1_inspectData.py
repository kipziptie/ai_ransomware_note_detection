import os, numpy
from scipy import stats
RAW_NOTE_DIR=os.path.dirname("./rawNotes/")
LENGTHS = []
UNREADABLE = 0
UNREADABLE_PATHS = []
files = os.listdir(RAW_NOTE_DIR)

textfiles = [f for f in files if f.lower().endswith(".txt")]
print("Textfiles: ", textfiles, "\n  Count: ", len(textfiles))

others = [f for f in files if not f in textfiles]
print("Not Textfiles: ", others, "\n  Count: ", len(others))

count = 0
for f in textfiles:
    filepath = os.path.join(RAW_NOTE_DIR, f)
    #print(filepath)

    with open(filepath, "r", encoding="utf8") as file:
        try:
            # Get the raw file content in text format
            content = file.read()
            #print(content)
            words = content.split()
            LENGTHS.append(len(words))
        except:
            print("File not readable: ", filepath)
            UNREADABLE = UNREADABLE + 1
            UNREADABLE_PATHS.append(filepath)

    count = count + 1

# Inspect the shape of the data
# print(LENGTHS)
MEAN = numpy.mean(LENGTHS)
MEDIAN = numpy.median(LENGTHS)
MODE = stats.mode(LENGTHS)
LENGTHS.sort()
SMALLEST = LENGTHS[0]
LENGTHS.sort(reverse=True)
LARGEST = LENGTHS[0]

def printResults():
    print("Total file count is: ", len(files))
    print("There are ",len(textfiles), " textFiles and ", len(others), " non-text files")
    print("  Of the non-text files, the most popular datatypes are <TODO - FILL THIS IN>\n")

    print(UNREADABLE, " files could not be opened. ")
    for file in UNREADABLE_PATHS:
        print("  ", file)

    print("\n## TEXTFILE DATA ##")
    print("Wordcounts...")
    print("  Mean: ", MEAN.round(), " Median: ", MEDIAN, " Mode: ", MODE.mode[0])
    print("  The shortest document was ", SMALLEST, " words long")
    print("  The longest document was ", LARGEST, " words long")

printResults()