import joblib, pickle
from sklearn.feature_extraction.text import CountVectorizer
from textProcessing import *
from buildCorpus import *
import numpy as np

DEBUG = False
SUPPRESS_BROKEN_IDEAS = True
ML_OUTPUT_DIR = 'ML_models_trained'

SAMPLE_FILES_DIR = os.path.dirname('./samples/')
SCIKIT_DIR = os.path.dirname("./scikit/")
scikitFiles = getDirContents(SCIKIT_DIR)

if DEBUG == True:
    print("Datasets to unpack: ", scikitFiles)

if not os.path.exists(SAMPLE_FILES_DIR):
    print(SAMPLE_FILES_DIR, " DOES NOT EXIST! Creating.. ")
    os.makedirs(SAMPLE_FILES_DIR)


def getSplits(file):
    with open(file, "rb") as f:
        splits = pickle.load(f)
    return splits


def getScikitSplits(file):
    filepath = os.path.join(SCIKIT_DIR, file[0])
    splits = getSplits(filepath)
    return splits


scikit_test_train_data = getScikitSplits(scikitFiles)
headings = []
for data in scikit_test_train_data:
    headings.append(data["headings"])

    if DEBUG == True:
        print("unpacked_headings: ", headings)

def get_dt(dataset_number):
    print("hello dt")
    path_dt = os.path.join(ML_OUTPUT_DIR, 'model_dt.joblib_') + str(dataset_number)
    print("Loading : ", path_dt)
    return joblib.load(path_dt)
def get_svm(dataset_number):
    print("hello svm")
    path_svm = os.path.join(ML_OUTPUT_DIR, 'model_svm.joblib_') + str(dataset_number)
    print("Loading : ", path_svm)
    return joblib.load(path_svm)

# Y_predictions=model_dt_loaded.predict(X_test[:10,])
# print('Test of loaded model: ', Y_test_pred_loaded)


### Step 1 ### Skim the dataset


### Step 2 ### clean the data
files = getDirContents(SAMPLE_FILES_DIR)
textFiles = getFilesByExtension(files, ".txt")

if DEBUG == True:
    print("Files to Process: \n", textFiles)

# Delete the contents of the clean note dir, to prevent mixing of data from a prior run
# This is a flaw, since the output module has a semi-hardcoded path in it.
deleteDirectory(CLEAN_NOTE_DIR)
cleanFiles(textFiles, path=SAMPLE_FILES_DIR)

### Step 3 ### Build Corpus
textFiles = getDirContents(CLEAN_NOTE_DIR)
# corpus = buildCorpus(textFiles)

# print(corpus)
array = []
sample_files = getDirContents(CLEAN_NOTE_DIR)
for file in sample_files:
    filepath = os.path.join(CLEAN_NOTE_DIR, str(file))
    if DEBUG == True:
        print("Opening: ", filepath)

    with open(filepath, "r", encoding="utf8") as file:
        try:
            # Get the raw file content in text format
            content = file.read()
            array.append(content)
        except:
            exit()

# <todo> array to pandas dataframe.. </todo>

# print(content)
# print(list(content))

# print("Submitted Samples: ", array)


#################################################################################
# Step 4 - Corpus to Count Vectorizer (with SAME FEATURES)
#
# This is a vector to mirror the same ones used during model training and testing. This is how
# new sample are tested.
#################################################################################
#
#   Model usage
#
for i in range(1, 4):
    #model_dt = joblib.load(os.path.join(ML_OUTPUT_DIR, 'model_dt.joblib_') + str(dataset_number))
    model_dt = get_dt(i)
    #model_svm = joblib.load(os.path.join(ML_OUTPUT_DIR, 'model_svm.joblib_') + str(dataset_number))
    model_svm = get_svm(i)

    # This vocabulary is taken from step 4. it's important, else the vectors tested won't align with the model.
    # <DONE> export these from step 4 and automagically import theses with the accompanying model.
    # vocabulary = ['address', 'contact', 'data', 'decrypt', 'encrypted', 'file', 'files', 'free', 'get', 'hours', 'key', 'mail', 'one', 'self', 'unique', 'url', 'us']
    vocabulary = headings[i-1]  # <todo> - process headings for all 3 models. This line lazily takes the most recent
    matrix = CountVectorizer(max_features=20, vocabulary=vocabulary, binary=True)

    meh = matrix.fit_transform(list(array)).toarray()

    if DEBUG == True:
        #################################################################################
        # Self-Test Corpus Collection
        #
        # A collection of sanity checking corpus to produce expected results. If the positive corpus does not test positive or the
        # negative corpus does not test negative, there is an issue which needs investigation.
        #################################################################################
        positive_corpus = [
            'this is the first sentence this is the second sentence decrypt decrypt encrypted files key software this is the third sentence this is the fourth sentence this is the fifth sentence']
        pos = matrix.fit_transform(positive_corpus).toarray()

        # A sample previously seen before. This sample MUST test negative.
        cheat_negative_corpus = [
            'Hemingway, Earnest  A farewell to arms  The old man and the sea  Wilbur, Dan  How not to read: harnessing the power of a literature free life  Robinson, Spider  Time Travelers strictly Cash  Remarque, erich marla  All Quiet on the Western Front   ']
        cheat = matrix.fit_transform(cheat_negative_corpus).toarray()

        # This sample SHOULD test negative
        negative_corpus = [
            'this is the first sentence this is the second sentence this is the third sentence this is the fourth sentence this is the fifth sentence']
        neg = matrix.fit_transform(negative_corpus).toarray()

        #################################################################################
        # Self-Test
        #
        # A collection of sanity checking corpus to produce expected results. If the positive corpus does not test positive or the
        # negative corpus does not test negative, there is an issue which needs investigation.
        #################################################################################
        # print("\n#################\n### SELF-TEST ###\n#################\n")
        # print("  Positive Input: ", pos)
        SVM_POSITIVE_TEST = model_svm.predict(pos)
        print("  SVM Positive Prediction: ", SVM_POSITIVE_TEST[0], " ", True)
        if SVM_POSITIVE_TEST != True:
            print("  Error!")

        # print("  Negative Input", neg)
        print("  SVM Negative Prediction", model_svm.predict(neg))
        # print("\n  Cheater Input", cheat)
        print("  SVM Cheater Negative Prediction", model_svm.predict(cheat))

        print("  DT POS ", model_dt.predict(pos))
        print("  DT neg ", model_dt.predict(neg))
        print("  DT cheat", model_dt.predict(cheat))

        print("\n################\n###   TEST   ###\n################\n")

        print("MEH", meh)
        print("SVM predict MEH ", model_svm.predict(meh))
        print("DT Predict MEH: ", model_dt.predict(meh))

        collection = np.append(meh, pos, axis=0)
        print("MEH + POS: ", model_dt.predict(collection))
        print("Meh\n", meh)
        print("\npos\n", pos)
        print("\nCollection\n", collection)

    #####################################################################################
    #
    #	Final Output Section
    #
    #####################################################################################
    print("The input Vector: ", meh, "\n")
    SVM_PREDICTION = model_svm.predict(meh)
    DT_PREDICTION = model_dt.predict(meh)
    print("Prediction Results: \n    DT  = ", DT_PREDICTION, "\n    SVM = ", SVM_PREDICTION)

    if SUPPRESS_BROKEN_IDEAS == False:
        print("Prediction Results: \n    DT  = ", DT_PREDICTION, "\n    SVM = ", SVM_PREDICTION)

        if DT_PREDICTION == '1':
            print(
                "\n######################################################\n\n###### Decision Trees Detected a Ransomware Note #####\n\n######################################################\n")

        if SVM_PREDICTION == '1':
            print(
                "\n######################################################\n\n# Support Vector Machines Detected a Ransomware Note #\n\n######################################################\n")

        if DT_PREDICTION == '1' and SVM_PREDICTION == '1':
            exit(1)

        elif DT_PREDICTION == '1' and SVM_PREDICTION == '0':
            exit(2)
        elif DT_PREDICTION == '0' and SVM_PREDICTION == '1':
            exit(3)
        elif DT_PREDICTION == '0' and SVM_PREDICTION == '0':
            exit(4)

        else:
            exit(0)
